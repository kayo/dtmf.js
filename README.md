# Intro

Simple DTMF generator and Goertzel-based decoder.
You can use this code to generate rfc2833 3.10 DTMF events and decode it in Javascript applications.
Library uses AMD (Asynchronous Module Definition) and tested with RequireJS.

# Usage with NodeJS

## Standalone

Create sound config:

    require(['sound'], function(Sound){
      var snd = Sound({
        format: 'S16LE', /* sample format */
        rate: 8000,      /* sample rate */
        channels: 1      /* sound channels */
      });
      ...
    });

Generate samples:

    require(['sound', 'dtmf'], function(Sound, DTMF){
      ...
      var gen = DTMF.Generator(snd, {
        pulse: 120, /* pulse duration in milliseconds */
        pause: 80   /* pause duration in milliseconds */
      });
      
      var samples = gen.gen(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#']);
      ...
      samples.data; /* result buffer with waveform */
      samples.toWav(); /* get RIFF wave container */
    });

Detect samples:

    require(['sound', 'dtmf'], function(Sound, DTMF){
      ...
      var det = DTMF.Detector(snd, {
        pulse: 40,    /* minimal pulse duration in milliseconds */
        silence: 0.02 /* silence threshold */
      });
      
      var samples = snd.fromWav(buffer); /* load samples from RIFF wave container */
      var events = det.det(samples);     /* detect events */
    });

## Streaming

Encode events to signal:

    require(['dtmf.stream'], function(DTMFS){
      var gen = DTMFS.Encoder({
        format: 'S16LE', /* sample format */
        rate: 8000,      /* sample rate */
        channels: 1,     /* sound channels */
        pulse: 120,      /* pulse duration in milliseconds */
        pause: 80        /* pause duration in milliseconds */
      });
      ...
      gen.pipe(sndout);
      gen.write('0123456789*#');
    });

Decode events from signal:

    require(['dtmf.stream'], function(DTMFS){
      var det = DTMFS.Decoder({
        format: 'S16LE', /* sample format */
        rate: 8000,      /* sample rate */
        channels: 1,     /* sound channels */
        pulse: 40,       /* minimal pulse duration in milliseconds */
        silence: 0.02,   /* silence threshold */
        dtmfMinRepeat: 1 /* repeat count of single event */
      });
      ...
      sndin.pipe(det);
      det.on('data', function(event){
        // 
      });
    });

# Licensing notes

This code available under MIT license.
