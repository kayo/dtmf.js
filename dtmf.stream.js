define(['util', 'stream', './dtmf', './sound'], function(util, Stream, DTMF, Sound){
  var min = Math.min,
      abs = Math.abs;

  function Decoder(opts){
    if(!(this instanceof Decoder)){
      return new Decoder(opts);
    }

    Stream.Transform.call(this, opts);

    this.setEncoding('ascii');

    this._cnt = opts.dtmfMinRepeat || 3;

    this._snd = new Sound(opts);
    this._det = DTMF.Detector(this._snd, opts);

    this._offset = 0;
    this._buffer = Buffer(this._det.pulseSamples * this._snd.size);
  }

  util.inherits(Decoder, Stream.Transform);

  Decoder.prototype._transform = function(data, enc, done){
    var size;

    for(; data.length; ){
      for(; this._offset < this._buffer.length && data.length > 0; ){
        size = min(data.length, this._buffer.length - this._offset);
        data.copy(this._buffer, this._offset);
        data = data.slice(size);
        this._offset += size;
      }

      if(this._offset >= this._buffer.length){
        //console.log('det', this._offset);
        var i = 0,
            code = this._det.det(this._snd.samples(this._buffer));

        if(!code.length){
          this._last_code = undefined;
        }else{
          for(; i < code.length; i++){
            if(!this._last_code){
              this._repeat_cnt = 0;
            }
            if(this._last_code == code[i]){
              if(++this._repeat_cnt == this._cnt){
                this.push(code[i]);
              }
            }else{
              this._repeat_cnt = 1;
            }
            this._last_code = code[i];
          }
        }

        this._offset = 0;
      }
    }

    done();
  };

  function Encoder(opts){
    if(!(this instanceof Encoder)){
      return new Encoder(opts);
    }

    Stream.Transform.call(this, opts);

    this._snd = new Sound(opts);
    this._gen = DTMF.Generator(this._snd, opts);
  }

  util.inherits(Encoder, Stream.Transform);

  Encoder.prototype._transform = function(data, enc, done){
    if(Buffer.isBuffer(data)){
      data = data.toString('ascii');
    }

    this.push(this._gen.gen(data.split('')).data);

    done();
  };

  return {
    Decoder: Decoder,
    Encoder: Encoder
  };
});
