define(function(){
  var intBits = { 8: 0, 16: 0, 32: 0, 64: 0 },
      floatBits = { 32: 0, 64: 0 };

  function parseFormat(fmt){
    var m = fmt.match(/^(S|U|F)(\d{1,2})?(?:\_)?(BE|LE)?/);

    if(!m){
      throw new Error('Unsupported format `' + fmt + '\'');
    }

    var f = m[1],
        b = m[2] || '32',
        e = m[3] || '',
        t, // type
        s = b >> 3, // size
        a = 1, // amp
        o = 0; // offset

    if(s === 1){
      e = '';
    }

    if(f === 'S' || f === 'U'){
      if(!(b in intBits)){
        throw new Error('Unsupported format `' + fmt + '\'');
      }
      t = 'Int';
      a = (2 << (b - 2)) - 1;
      if(f === 'U'){
        t = 'U' + t;
        o = a;
      }
      t += b;
    }

    if(f === 'F'){
      if(!(b in floatBits)){
        throw new Error('Unsupported format `' + fmt + '\'');
      }
      t = b == '64' ? 'Double' : 'Float';
    }

    t += e;

    var r = Buffer.prototype['read' + t],
        w = Buffer.prototype['write' + t];

    return {
      bits: b | 0,
      bytes: s,
      read: function(buffer, offset){
        return (r.call(buffer, offset * s) - o) / a;
      },
      write: f === 'F' ? function(buffer, offset, value){
        w.call(buffer, a * value + o, offset * s);
      } : function(buffer, offset, value){
        w.call(buffer, (a * value) | 0 + o, offset * s);
      }
    }
  }

  function Sound(opts){
    if(!(this instanceof Sound)){
      return new Sound(opts);
    }

    this.format = opts.sampleFormat || opts.format || 'S16LE';
    this.rate = opts.sampleRate || opts.rate || 44100;
    this.period = 1000 / this.rate;
    this.channels = opts.channels || 1;
    this.inter = opts.inter || !opts.noninter;

    var info = parseFormat(this.format);

    this.bits = info.bits;
    this.bytes = info.bytes;
    this.size = this.channels * this.bytes;

    this._read = info.read;
    this._write = info.write;
  }

  Sound.prototype = {
    constructor: Sound,
    samples: function(samples){
      return new Samples(this, samples);
    },
    sample: function(samples, offset, channel){
      channel = channel || 0;
      return (this.inter
             ? (offset * this.channels + channel)
             : (samples * channel + offset));
    }
  };

  Sound.fromWav = function(ibuf){
    var opts = RIFF_ParseHeader(ibuf),
        sound = Sound(opts),
        buffer = sound.samples(RIFF_ExtractData(ibuf));

    return buffer;
  };

  function Samples(sound, samples){
    if(!(this instanceof Samples)){
      return new Samples(sound, samples);
    }

    this.sound = sound;

    if(Buffer.isBuffer(samples)){
      this.length = (samples.length / sound.size) | 0;
      this.data = samples;
    }else{
      this.length = samples;
      this.data = Buffer(sound.size * samples);
    }

    this.period = sound.period * this.length;
    this.bytes = this.length * sound.size;
  }

  Samples.prototype = {
    constructor: Samples,
    sample: function(offset, channel){
      return this.sound.sample(this.length, offset, channel);
    },
    read: function(offset, channel){
      return this.sound._read(this.data, this.sample(offset, channel));
    },
    write: function(value, offset, channel){
      this.sound._write(this.data, this.sample(offset, channel), value);
    },
    slice: function(start, end){
      start = start || 0;
      end = end || this.length;

      return this.sound.samples(this.data.slice(start * this.size, end * this.size));
    },
    concat: function(piece){

    },
    normalize: function(){
      var c, s, amp = 0;

      for(c = 0; c < this.channels; c++){
        for(s = 0; s < this.length; s++){
          amp = Math.max(this.read(s, c), amp);
        }
      }

      amp = 1 / amp;

      for(c = 0; c < this.channels; c++){
        for(s = 0; s < this.length; s++){
          this.write(this.read(s, c), s, c);
        }
      }
    },
    toWav: function(){
      return Buffer.concat([
        RIFF_FormatHeader(this.sound, this.length),
        this.data
      ]);
    }
  };

  /*
   * RIFF utils
   */

  var RIFF_DataOffset = 44;

  function RIFF_ParseHeader(buf){
    if('RIFF' !== buf.toString('ascii', 0, 4)
     ||'WAVE' !== buf.toString('ascii', 8, 12)
     ||'fmt ' !== buf.toString('ascii', 12, 16)
     ||'data' !== buf.toString('ascii', 36, 40)){
      throw new Error('Invalid header');
    }

    return {
      channels: buf.readUInt16LE(22),
      rate: buf.readUInt32LE(24),
      bits: buf.readUInt16LE(34),
      samples: buf.readUInt32LE(40) / buf.readUInt16LE(32)
    };
  }

  function RIFF_ExtractData(buf){
    return buf.slice(RIFF_DataOffset);
  }

  function RIFF_FormatHeader(sound, samples){
    var buf = new Buffer(RIFF_DataOffset),

        byteRate = sound.rate * sound.size,
        dataChunkSize = samples * sound.size;

    buf.write('RIFF');
    buf.writeUInt32LE(36 + dataChunkSize, 4); /* content size */
    buf.write('WAVE', 8);

    /* `fmt' chunk */
    buf.write('fmt ', 12);
    buf.writeUInt32LE(16, 16); /* chunk size */
    buf.writeUInt16LE(1, 20); /* audio data format: uncompressed linear quantized PCM data */
    buf.writeUInt16LE(sound.channels, 22);
    buf.writeUInt32LE(sound.rate, 24);
    buf.writeUInt32LE(byteRate, 28);
    buf.writeUInt16LE(sound.size, 32);
    buf.writeUInt16LE(sound.bits, 34);

    /* `data' chunk */
    buf.write('data', 36);
    buf.writeUInt32LE(dataChunkSize, 40);

    return buf;
  }

  return Sound;
});
