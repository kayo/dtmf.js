define(function(){
  var M = Math,
      pi = M.PI,
      pi2 = 2 * pi,
      min = M.min,
      abs = M.abs,
      cos = M.cos,

  /*
|  Hz | 1209 | 1336 | 1477 | 1633 |
|-----+------+------+------+------|
| 697 |    1 |    2 |    3 | A    |
| 770 |    4 |    5 |    6 | B    |
| 852 |    7 |    8 |    9 | C    |
| 941 |    * |    0 |    # | D    |
 */
      freq = [
        697, 770, 852, 941, /* low frequency (rows) */
        1209, 1336, 1477, 1633 /* high frequency (columns) */
      ],

  /* rfc2833 3.10 DTMF Events */
      idx2evt = [
        ['1', '2', '3', 'A'],
        ['4', '5', '6', 'B'],
        ['7', '8', '9', 'C'],
        ['*', '0', '#', 'D']
      ],

      evt2idx = {
        '1': [0, 4], '2': [0, 5], '3': [0, 6], 'A': [0, 7],
        '4': [1, 4], '5': [1, 5], '6': [1, 6], 'B': [1, 7],
        '7': [2, 4], '8': [2, 5], '9': [2, 6], 'C': [2, 7],
        '*': [3, 4], '0': [3, 5], '#': [3, 6], 'D': [3, 7]
      };

  function validateEvent(event){
    if(!evt2idx[event]){
      throw new Error('Unsupported event `' + event + '\'');
    }

    return event;
  }

  function Generator(sound, opts){
    if(!(this instanceof Generator)){
      return new Generator(sound, opts);
    }

    opts = opts || {};

    this.sound = sound;

    this.pulse = opts.pulse || 120;
    this.pause = opts.pause || 80;

    this.pulseSamples = (this.pulse / sound.period) | 0;
    this.pauseSamples = (this.pause / sound.period) | 0;

    this.freq = new Array(freq.length);

    for(var i = 0; i < freq.length; i++){
      this.freq[i] = freq[i] * pi2 / 1000;
    }
  }

  function genEvent(self, event, buffer, start, samples, period){
    var i = start,
        end = start + samples,
        idx = evt2idx[event],
        row = self.freq[idx[0]],
        col = self.freq[idx[1]],
        time = 0;

    for(; i < end; time += period){
      buffer.write(0.5 * (0.8 * cos(row * time) - cos(col * time)), i++, 0);
    }
  }

  function genSilence(buffer, start, samples){
    var i = start,
        end = start + samples;

    for(; i < end; ){
      buffer.write(0, i++, 0);
    }
  }
  
  Generator.prototype = {
    gen: function(codes_or_events){
      var i,
          pulseSamples = this.pulseSamples,
          pauseSamples = this.pauseSamples,
          eventSamples = pulseSamples + pauseSamples,
          start = 0,
          events = new Array(codes_or_events.length);

      for(i = 0; i < events.length; i++){
        events[i] = validateEvent(codes_or_events[i]);
      }

      var buffer = this.sound.samples(events.length * eventSamples);

      for(i = 0; i < events.length; start += eventSamples){
        genEvent(this, events[i++], buffer, start, pulseSamples, this.sound.period);
        genSilence(buffer, start + pulseSamples, pauseSamples);
      }

      return buffer;
    }
  };

  function Detector(sound, opts){
    if(!(this instanceof Detector)){
      return new Detector(sound, opts);
    }

    opts = opts || {};

    this.sound = sound;

    this.silence = 0.02; /* silence threshold */

    this.pulse = opts.pulse || 40; /* time window in ms for recognition */

    this.pulseSamples = (this.pulse / sound.period) | 0;

    var count = this.pulseSamples, /* samples for recognition */
        interval = this.pulse / 1000,

        alpha = this.alpha = new Array(freq.length);

    for(var i = 0; i < freq.length; i++){
      alpha[i] = 2 * cos(pi2 * (0.5 + (freq[i] * interval) | 0) / count);
    }
  }

  function detGoertzel(alpha, buffer, start, count){
    var i,
        n,
        m,
        a,
        end = start + count,
        S0,
        S1,
        S2,
        M = new Array(alpha.length);

    for(i = 0; i < alpha.length; i++){
      a = alpha[i];

      S1 = 0;
      S2 = 0;

      for(n = start; n < end; n++){
        S0 = a * S1 - S2 + buffer.read(n, 0);
        S2 = S1;
        S1 = S0;
      }

      M[i] = S1 * S1 + S2 * S2 - S1 * S2 * a;
    }

    return M;
  }

  function findFreq(amps, start, end){
    var i = start,
        M = 0,
        I,
        m = 0;

    for(; i < end; i++){
      if(amps[i] > M){
        m = M;
        M = amps[i];
        I = i;
      }else if(amps[i] > m){
        m = amps[i];
      }
    }

    if(M > m * 10){
      return I;
    }

    return -1;
  }

  function detFragment(alpha, buffer, start, count){
    var amps = detGoertzel(alpha, buffer, start, count),
        row = findFreq(amps, 0, 4),
        col = findFreq(amps, 4, 8);

    if(row >= 0 && row < 4 && col > 3 && col < 8){
      return idx2evt[row][col-4];
    }

    return null;
  }

  function detAverage(buffer, start, count){
    var i = start,
        end = start + count,
        avg = 0.0;

    for(; i < end; ){
      avg += abs(buffer.read(i++, 0));
    }

    return avg / count;
  }

  Detector.prototype = {
    det: function(samples){
      var i = 0,
          count = this.pulseSamples,
          length = samples.length,
          event,
          events = [];

      for(; i < length; i += count){
        if(detAverage(samples, i, count) > this.silence){
          event = detFragment(this.alpha, samples, i, count);
          if(event){
            //if(!events.length || events[events.length - 1] !== event){
            events.push(event);
            //}
          }
        }
      }

      return events;
    }
  };

  return {
    Generator: Generator,
    Detector: Detector
  };
});
